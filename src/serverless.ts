/* istanbul ignore file */
import { APIGatewayProxyEvent, Context } from 'aws-lambda';
import awsServerlessExpress from 'aws-serverless-express';
import App from './infra/app';

const binaryMimeTypes = [
    'application/octet-stream',
    'font/eot',
    'font/opentype',
    'font/otf',
    'image/jpeg',
    'image/png',
    'image/svg+xml',
];
const server = awsServerlessExpress.createServer(new App().expressApp, undefined, binaryMimeTypes);

export function handler(event: APIGatewayProxyEvent, context: Context): void {
    awsServerlessExpress.proxy(server, event, context);
}
