import { Router } from 'express';
import { getLogger } from '../../../logger';

const logger = getLogger('userHandler');

interface UserResp {
    name: string;
    token: string;
}

export default (): Router => {
    return Router().get('/', (req, res) => {
        logger.debug('GET / request');
        const response: UserResp = { name: 'Dennis Probst', token: 'e4fa4b9d-5bb5-442b-a7a7-ae038929ef7a' };
        res.json(response);
    });
};
