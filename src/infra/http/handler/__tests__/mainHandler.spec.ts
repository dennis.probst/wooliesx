import { mock } from 'jest-mock-extended';
import supertest from 'supertest';
import { ProductService } from '../../../../application/productService';
import { TrolleyService } from '../../../../application/trolleyService';
import Product from '../../../../model/product';
import { StatusCode } from '../../../../model/statusCode';
import { ProductDto } from '../../DTOs/productDto';
import ExpressApp from '../../expressApp';
import MainHandler from '../mainHandler';

describe('mainHandler', () => {
    const mockProductService = mock<ProductService>();
    const mockTrolleyService = mock<TrolleyService>();
    const productHandler = new MainHandler(mockProductService, mockTrolleyService);
    const mockProduct = Product.create({ name: 'product name', price: 5, quantity: 4 });
    const express = ExpressApp.bootstrap({ '/': productHandler.router() });

    beforeEach(() => {
        jest.resetAllMocks();
    });

    describe('GET /sort', () => {
        it('responds with a list of product DTOs', async () => {
            mockProductService.listProducts.mockResolvedValue([mockProduct]);
            const { body } = await supertest(express).get('/sort').expect(StatusCode.Ok);
            expect(body).toEqual<ProductDto[]>([
                {
                    name: 'product name',
                    price: 5,
                    quantity: 4,
                },
            ]);
        });

        it('responds with an empty list when there are not products', async () => {
            mockProductService.listProducts.mockResolvedValue([]);
            const { body } = await supertest(express).get('/sort').expect(StatusCode.Ok);
            expect(body).toEqual<ProductDto[]>([]);
        });

        it.todo('calls next() and return Internal Server Error through error handler');
    });

    describe('POST /trolleyTotal', () => {
        it('calculates trolley total and responds with a total', async () => {
            mockTrolleyService.calculateTotal.mockReturnValue(4);
            const { body } = await supertest(express)
                .post('/trolleyTotal')
                .set('Content-Type', 'application/json')
                .send({
                    Products: [{ Name: 'string1', Price: 10 }],
                    Specials: [{ Quantities: [{ Name: 'string1', Quantity: 5 }], Total: 4 }],
                    Quantities: [{ Name: 'string1', Quantity: 5 }],
                });
            expect(body).toEqual(4);
            expect(mockTrolleyService.calculateTotal).toHaveBeenCalledWith({
                products: [{ name: 'string1', price: 10 }],
                specials: [{ quantities: [{ name: 'string1', quantity: 5 }], total: 4 }],
                quantities: [{ name: 'string1', quantity: 5 }],
            });
        });

        it.todo('throws invalidArgument error when wrong json format received');
    });
});
