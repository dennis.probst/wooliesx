import supertest from 'supertest';
import { StatusCode } from '../../../../model/statusCode';
import ExpressApp from '../../expressApp';
import userHandler from '../userHandler';

describe('userHandler', () => {
    const express = ExpressApp.bootstrap({ '/user': userHandler() });

    describe('GET /', () => {
        it('responds with name and token', async () => {
            const { body } = await supertest(express).get('/user').expect(StatusCode.Ok);
            expect(body).toEqual({
                name: 'Dennis Probst',
                token: 'e4fa4b9d-5bb5-442b-a7a7-ae038929ef7a',
            });
        });
    });
});
