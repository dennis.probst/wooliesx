import { Router } from 'express';
import { OrderProductBy, ProductService } from '../../../application/productService';
import { TrolleyService } from '../../../application/trolleyService';
import { getLogger } from '../../../logger';
import InvalidArgumentError from '../../../model/errors/invalidArgumentError';
import { productToJSON } from '../DTOs/productDto';
import { trolleyFromJSON } from '../DTOs/trolleyDTO';

const logger = getLogger('mainHandler');

export default class MainHandler {
    constructor(private productService: ProductService, private trolleyService: TrolleyService) {}

    router(): Router {
        const handler = Router();
        handler.get(
            '/sort',
            async (req, res, next): Promise<void> => {
                logger.debug('GET /sort request');
                const { sortOption } = req.query as { sortOption: OrderProductBy };
                try {
                    const products = await this.productService.listProducts({ orderBy: sortOption });
                    res.json(products.map(productToJSON));
                } catch (err) /* istanbul ignore next: for exercies */ {
                    next(err);
                }
                return;
            },
        );

        handler.post(
            '/trolleyTotal',
            async (req, res, next): Promise<void> => {
                logger.debug('POST /trolleyTotal request');
                const { body } = req;
                logger.debug('body: %O', body);
                let trolley;
                try {
                    trolley = trolleyFromJSON(body);
                } catch (err) /* istanbul ignore next: for code exercise */ {
                    logger.trace('body', body);
                    next(new InvalidArgumentError('data not valid'));
                    return;
                }
                try {
                    const total = await this.trolleyService.calculateTotal(trolley);
                    res.json(total);
                } catch (err) /* istanbul ignore next: for exercies */ {
                    next(err);
                }
                return;
            },
        );

        return handler;
    }
}
