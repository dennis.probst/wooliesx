import got from 'got';
import { ProductsResourceClient } from '../../../application/productsResourceClient';
import { resouceApiConfig } from '../../../config';
import { getLogger } from '../../../logger';
import InternalServerError from '../../../model/errors/internalServerError';
import Product from '../../../model/product';
import ShoppersHistory from '../../../model/shoppersHistory';
import { productFromJSON } from '../DTOs/productDto';
import { shopperHistoryFromJSON } from '../DTOs/shopperHistoryDto';

const logger = getLogger('productsResourceClientImpl');

export default class ProductsResourceClientImpl implements ProductsResourceClient {
    async listProducts(): Promise<Product[]> {
        logger.debug('Requesting products from resource API');
        const uri = `${resouceApiConfig.host}/api/resource/products`;
        try {
            const { body } = await got(uri, {
                searchParams: { token: resouceApiConfig.token },
                responseType: 'json',
                retry: 0,
            });
            const productDtos = body as Record<string, unknown>[];
            return productDtos.map(productFromJSON);
        } catch (err) {
            logger.error(`Http GET request error URI %s, error: %s : %s`, uri, err, err.stack);
            throw new InternalServerError('Downstream resource unreachable');
        }
    }

    async listShopperHistories(): Promise<ShoppersHistory[]> {
        logger.debug('Requesting shoppers history from resource API');
        const uri = `${resouceApiConfig.host}/api/resource/shopperHistory`;
        try {
            const { body } = await got(uri, {
                searchParams: { token: resouceApiConfig.token },
                responseType: 'json',
                retry: 0,
            });
            const shopperHistoriesDto = body as Record<string, unknown>[];
            return shopperHistoriesDto.map(shopperHistoryFromJSON);
        } catch (err) {
            logger.error(`Http GET request error URI %s, error: %s : %s`, uri, err, err.stack);
            throw new InternalServerError('Downstream resource unreachable');
        }
    }
}
