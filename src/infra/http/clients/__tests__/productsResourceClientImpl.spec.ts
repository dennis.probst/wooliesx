jest.mock('../../../../config', () => ({
    ...(jest.requireActual('../../../../config') as Record<string, unknown>),
    resouceApiConfig: {
        host: 'http://somewhere.com',
        token: 'mock-token',
    },
}));

import nock from 'nock';
import { ProductsResourceClient } from '../../../../application/productsResourceClient';
import InternalServerError from '../../../../model/errors/internalServerError';
import Product from '../../../../model/product';
import ShoppersHistory from '../../../../model/shoppersHistory';
import { StatusCode } from '../../../../model/statusCode';
import { ProductDto } from '../../DTOs/productDto';
import { ShopperHistoryDto } from '../../DTOs/shopperHistoryDto';

import ProductsResourceClientImpl from '../productsResourceClientImpl';

describe('productsResourceClientImpl', () => {
    let productsResourceClient: ProductsResourceClient;

    beforeEach(() => {
        productsResourceClient = new ProductsResourceClientImpl();
    });

    describe('listProducts', () => {
        const mockProduct: ProductDto = { name: 'Test Product A', quantity: 0, price: 99.99 };

        it('requests products from the resource api', async () => {
            nock('http://somewhere.com')
                .get('/api/resource/products')
                .query({ token: 'mock-token' })
                .reply(StatusCode.Ok, [mockProduct]);
            const products = await productsResourceClient.listProducts();
            expect(products).toEqual([Product.create({ name: 'Test Product A', quantity: 0, price: 99.99 })]);
        });

        it('throws internal server error when something errors', async () => {
            expect.assertions(1);
            nock('http://somewhere.com')
                .get('/api/resource/products')
                .query({ token: 'mock-token' })
                .reply(StatusCode.InternalServerError, new Error('test error'));
            try {
                await productsResourceClient.listProducts();
            } catch (err) {
                expect(err).toBeInstanceOf(InternalServerError);
            }
        });
    });

    describe('shopperHistory', () => {
        const shopperHistoryDto: ShopperHistoryDto = {
            customerId: 2,
            products: [{ name: 'product name', price: 1, quantity: 2 }],
        };

        it('requests the shopper history from the resource API', async () => {
            nock('http://somewhere.com')
                .get('/api/resource/shopperHistory')
                .query({ token: 'mock-token' })
                .reply(StatusCode.Ok, [shopperHistoryDto]);

            const shopperHistory = await productsResourceClient.listShopperHistories();
            expect(shopperHistory).toEqual([
                ShoppersHistory.create({
                    customerId: 2,
                    products: [{ name: 'product name', price: 1, quantity: 2 }],
                }),
            ]);
        });

        it('throws internal server error when something errors', async () => {
            expect.assertions(1);
            nock('http://somewhere.com')
                .get('/api/resource/shopperHistory')
                .query({ token: 'mock-token' })
                .reply(StatusCode.InternalServerError, new Error('test error'));
            try {
                await productsResourceClient.listShopperHistories();
            } catch (err) {
                expect(err).toBeInstanceOf(InternalServerError);
            }
        });
    });
});
