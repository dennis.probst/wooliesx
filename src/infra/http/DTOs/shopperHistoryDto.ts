/* istanbul ignore file: equal to productsDTO */

import Product from '../../../model/product';
import ShoppersHistory from '../../../model/shoppersHistory';

export interface ShopperHistoryProductDto {
    name?: string;
    price?: number;
    quantity?: number;
}

export interface ShopperHistoryDto {
    customerId?: number;
    products?: ShopperHistoryProductDto[];
}

export function shopperHistoryFromJSON(json: Record<string, unknown>): ShoppersHistory {
    const { customerId, products: shopperHistoryProductDto } = json as ShopperHistoryDto;
    return ShoppersHistory.create({
        customerId,
        products: shopperHistoryProductDto ? shopperHistoryProductDto.map(productFromShopperHistoryProductJSON) : [],
    });
}

function productFromShopperHistoryProductJSON(shopperHistoryProductDto: ShopperHistoryProductDto): Product {
    const { name, price, quantity } = shopperHistoryProductDto;
    return Product.create({ name, price, quantity });
}
