import { Trolley } from '../../../../model/trolley';
import { trolleyFromJSON } from '../trolleyDTO';

describe('trolleyDto', () => {
    describe('trolleyFromJSON', () => {
        it('copies a maximal transferred json into a Trolley object', () => {
            const trolleyJson = {
                Products: [
                    { Name: '1', Price: 2 },
                    { Name: '2', Price: 5 },
                ],
                Specials: [
                    {
                        Quantities: [
                            { Name: '1', Quantity: 3 },
                            { Name: '2', Quantity: 0 },
                        ],
                        Total: 5,
                    },
                    {
                        Quantities: [
                            { Name: '1', Quantity: 1 },
                            { Name: '2', Quantity: 2 },
                        ],
                        Total: 10,
                    },
                ],
                Quantities: [
                    {
                        Name: '1',
                        Quantity: 3,
                    },
                    {
                        Name: '2',
                        Quantity: 2,
                    },
                ],
            };
            expect(trolleyFromJSON(trolleyJson)).toEqual<Trolley>({
                products: [
                    { name: '1', price: 2 },
                    { name: '2', price: 5 },
                ],
                specials: [
                    {
                        quantities: [
                            { quantity: 3, name: '1' },
                            { quantity: 0, name: '2' },
                        ],
                        total: 5,
                    },
                    {
                        quantities: [
                            { quantity: 1, name: '1' },
                            { quantity: 2, name: '2' },
                        ],
                        total: 10,
                    },
                ],
                quantities: [
                    { quantity: 3, name: '1' },
                    { quantity: 2, name: '2' },
                ],
            });
        });

        it('copies a minimal transferred json into a Trolley object', () => {
            const trolleyJson = {
                Products: [],
                Specials: [
                    { Quantities: [], Total: 0 },
                    { Quantities: [], Total: 0 },
                    { Quantities: [], Total: 0 },
                ],
                Quantities: [],
            };
            expect(trolleyFromJSON(trolleyJson)).toEqual<Trolley>({
                products: [],
                specials: [
                    { quantities: [], total: 0 },
                    { quantities: [], total: 0 },
                    { quantities: [], total: 0 },
                ],
                quantities: [],
            });
        });
    });
});
