import Product from '../../../../model/product';
import { productFromJSON } from '../productDto';

describe('productDto', () => {
    describe('productFromJSON', () => {
        it('maps a maximal JSON transfer object to a product', () => {
            const productDto = {
                name: 'name',
                quantity: 4,
                price: 5,
            };
            expect(productFromJSON(productDto)).toEqual(Product.create({ name: 'name', quantity: 4, price: 5 }));
        });

        it('maps a minimal JSON transfer object to a product', () => {
            const productDto = {};
            expect(productFromJSON(productDto)).toEqual(Product.create({}));
        });
    });

    describe('productToJSON', () => {
        it.todo('maps a minimal product entity to a JSON transfer object');

        it.todo('maps a maximal product entity to a JSON transfer object');
    });
});
