/* istanbul ignore file: equivalent to productDto.ts tests */
import { ProductQuantity, Special, Trolley, TrolleyProduct } from '../../../model/trolley';

export interface TrolleyDTO {
    Products: TrolleyProductDTO[];
    Specials: SpecialDTO[];
    Quantities: ProductQuantityDTO[];
}

export interface TrolleyProductDTO {
    Name?: string;
    Price?: number;
}

export interface SpecialDTO {
    Quantities?: ProductQuantityDTO[];
    Total?: number;
}

export interface ProductQuantityDTO {
    Name?: string;
    Quantity?: number;
}

export function trolleyFromJSON(json: Record<string, unknown>): Trolley {
    const { Products, Specials, Quantities } = (json as unknown) as TrolleyDTO;
    return {
        products: Products.map(trolleyProductFromJSON),
        specials: Specials.map(specialFromJSON),
        quantities: Quantities.map(quantitiesFromJSON),
    };
}

export function trolleyProductFromJSON(json: TrolleyProductDTO): TrolleyProduct {
    const { Name, Price } = json as TrolleyProductDTO;
    return { name: Name, price: Price };
}

function specialFromJSON(json: SpecialDTO): Special {
    const { Quantities, Total } = json;
    return {
        quantities: Quantities ? Quantities.map(quantitiesFromJSON) : undefined,
        total: Total,
    };
}

function quantitiesFromJSON(json: ProductQuantityDTO): ProductQuantity {
    const { Quantity, Name } = json;
    return { quantity: Quantity, name: Name };
}
