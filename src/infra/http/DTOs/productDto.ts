import Product from '../../../model/product';

export interface ProductDto {
    name?: string;
    price?: number;
    quantity?: number;
}

export function productFromJSON(json: Record<string, unknown>): Product {
    const { name, quantity, price } = json as ProductDto;
    return Product.create({
        name,
        price: price !== undefined ? price : undefined,
        quantity: quantity !== undefined ? quantity : undefined,
    });
}

/* istanbul ignore next: for exercise equivalent to above */
export function productToJSON(product: Product): ProductDto {
    const { name, quantity, price } = product;
    return {
        name,
        quantity,
        price,
    };
}
