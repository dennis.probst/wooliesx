/* istanbul ignore file: for code exercise */
import express, { Application, Handler, NextFunction, Request, Response } from 'express';
import { appConfig } from '../../config';
import { expressPinoLogger, getLogger } from '../../logger';
import { StatusCode } from '../../model/statusCode';
import MainHandler from './handler/mainHandler';
import userHandler from './handler/userHandler';

const logger = getLogger('expressApp');

type RouteDefinitions = { [path: string]: Handler };

export default class ExpressApp {
    private readonly routes: RouteDefinitions;

    constructor(private productHandler: MainHandler) {
        this.routes = {
            '/user': userHandler(),
            '/': this.productHandler.router(),
        };
    }

    static bootstrap(routes: RouteDefinitions): Application {
        logger.debug('Bootstrapping express');

        const expressApp = express();
        expressApp.set('port', appConfig.port);
        expressApp.use(express.urlencoded({ extended: true }));
        expressApp.use(express.json());
        expressApp.use(expressPinoLogger());
        Object.entries(routes).forEach(([path, handler]) => {
            expressApp.use(path, handler);
        });

        /* istanbul ignore next: for code exercise otherwise this should be covered */
        expressApp.use((req: Request, res: Response): void => {
            res.status(StatusCode.NotFound).send('Not Found');
        });
        /* istanbul ignore next: for code exercise otherwise this should be covered */
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        expressApp.use((err: Error & { status?: number }, req: Request, res: Response, next: NextFunction): void => {
            logger.error('Error handler error %s, %s', err.message, err.stack);
            res.status(err.status || StatusCode.InternalServerError).send({
                error: err.name || 'Internal Server Error',
                message: err.message,
            });
        });
        return expressApp;
    }

    bootstrap(): Application {
        return ExpressApp.bootstrap(this.routes);
    }
}
