/* istanbul ignore file: for code exercise */
import { Application } from 'express';
import ProductServiceImpl from '../application/productServiceImpl';
import TrolleyServiceImpl from '../application/trolleyServiceImpl';
import ProductsResourceClientImpl from './http/clients/productsResourceClientImpl';
import ExpressApp from './http/expressApp';
import MainHandler from './http/handler/mainHandler';

export default class App {
    readonly expressApp: Application;

    constructor() {
        const productsResourceClient = new ProductsResourceClientImpl();
        this.expressApp = new ExpressApp(
            new MainHandler(new ProductServiceImpl(productsResourceClient), new TrolleyServiceImpl()),
        ).bootstrap();
    }

    start(cb: (error?: Error | null, port?: number) => void): void {
        this.expressApp.listen(this.expressApp.get('port'), () => cb(null, this.expressApp.get('port')));
    }
}
