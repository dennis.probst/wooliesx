import { getLogger } from '../logger';
import { ProductQuantity, Special, Trolley, TrolleyProduct } from '../model/trolley';
import { TrolleyService } from './trolleyService';

const logger = getLogger('trolleyServiceImpl');

export default class TrolleyServiceImpl implements TrolleyService {
    calculateTotal(trolley: Trolley): number {
        const { quantities, products, specials } = trolley;
        return this.workoutMinRecursive(quantities, specials, products);
    }

    workoutMinRecursive(productQuantities: ProductQuantity[], specials: Special[], products: TrolleyProduct[]): number {
        const applicableSpecials = specials.filter((special) => this.isApplicable(productQuantities, special));
        if (applicableSpecials.length === 0) {
            return this.calculateMax(productQuantities, products);
        }
        const restApplicableSpecials = [...applicableSpecials];
        const specialsPermutations: number[] = applicableSpecials.map(() => {
            const special = restApplicableSpecials.shift() as Special;
            const restProductQuantities = this.applySpecial(productQuantities, special);
            const maxOfSpecial = this.calculateMax(special.quantities as ProductQuantity[], products);
            const headMin = Math.min(maxOfSpecial, special.total as number);
            const calcTotal =
                headMin + this.workoutMinRecursive(restProductQuantities, restApplicableSpecials, products);
            restApplicableSpecials.push(special as Special);
            return calcTotal;
        });
        return Math.min(...specialsPermutations);
    }

    /* istanbul ignore next: test indicated in spec file but not implemented */
    calculateMax(quantities: ProductQuantity[], products: TrolleyProduct[]): number {
        const pricesEachProduct: number[] = quantities.map((quantity) => {
            const { name, quantity: quantityWant } = quantity;
            if (!quantityWant) {
                return 0;
            }
            const product = products.find((product) => product.name === name);
            if (!product) {
                return 0;
            }
            const { price } = product;
            if (!price) {
                return 0;
            }
            return quantityWant * price;
        });
        return pricesEachProduct.reduce((a, b) => a + b);
    }

    applySpecial(productQuantities: ProductQuantity[], special: Special): ProductQuantity[] {
        const { quantities: specialQuantities } = special;
        const newProdQuantities = JSON.parse(JSON.stringify(productQuantities)) as ProductQuantity[];
        (specialQuantities as ProductQuantity[]).forEach((specialQuantity) => {
            const prodQuantity = newProdQuantities.find((prodQuantity) => prodQuantity.name === specialQuantity.name);
            if (
                prodQuantity?.quantity === undefined ||
                specialQuantity?.quantity === undefined ||
                specialQuantity.quantity > prodQuantity.quantity
            ) {
                throw new Error('this is an error, special not applied');
            }
            prodQuantity.quantity = prodQuantity.quantity - specialQuantity.quantity;
        });
        return newProdQuantities;
    }

    isApplicable(productQuantities: ProductQuantity[], special: Special): boolean {
        const { quantities: specialQuants, total } = special;
        if (!specialQuants || !total) {
            return false;
        }
        for (const specialQuant of specialQuants) {
            if (!specialQuant.name) {
                logger.debug('skipping special quantity without name');
                return false;
            }
            if (specialQuant.quantity === undefined) {
                logger.debug('skipping special productQuantity without quantity');
                return false;
            }
            if (
                !productQuantities.find((productQuantity) => {
                    if (productQuantity.quantity === undefined) {
                        return false;
                    }
                    return (
                        productQuantity.name === specialQuant.name &&
                        productQuantity.quantity >= (specialQuant.quantity as number)
                    );
                })
            ) {
                return false;
            }
        }
        return true;
    }
}
