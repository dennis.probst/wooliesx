import { getLogger } from '../../logger';
import Product from '../../model/product';
import ShoppersHistory from '../../model/shoppersHistory';
import { compareNumbers } from '../../utils';
import { OrderProductBy } from '../productService';

const logger = getLogger('sortProducts');

export default class SortProduct {
    static byAttribute(products: Product[], orderBy = OrderProductBy.NameAscending): Product[] {
        switch (orderBy) {
            case OrderProductBy.NameAscending:
                return products
                    .filter((product) => product.name !== undefined)
                    .sort((a, b) => (a.name as string).localeCompare(b.name as string));
            case OrderProductBy.NameDescending: {
                return products
                    .filter((product) => product.name !== undefined)
                    .sort((a, b) => (b.name as string).localeCompare(a.name as string));
            }
            case OrderProductBy.PriceDescending:
                return products
                    .filter((product) => product.price !== undefined)
                    .sort((a, b) => compareNumbers(b.price as number, a.price as number));
            case OrderProductBy.PriceAscending:
                return products
                    .filter((product) => product.price !== undefined)
                    .sort((a, b) => compareNumbers(a.price as number, b.price as number));
        }
        return products;
    }

    static byPopularity(shopperHistories: ShoppersHistory[], products: Product[]): Product[] {
        logger.debug('sorting shopperHistory products');
        const popularities = new Map<string, number>();
        const historiesWithProducts = shopperHistories.filter((shopperHistory) => shopperHistory.products?.length);
        const shoppedProducts = historiesWithProducts
            .map((historiesWithProducts) => historiesWithProducts.products)
            .flat() as Product[];
        shoppedProducts.forEach((product) => {
            const { name, quantity, price } = product;
            if (!name || quantity === undefined || price === undefined) {
                return;
            }
            if (popularities.has(name)) {
                popularities.set(name, (popularities.get(name) as number) + quantity * price);
                return;
            }
            popularities.set(name, quantity);
        });
        const sorted = Array.from(popularities.entries()).sort((a, b) => compareNumbers(b[1], a[1]));
        const productsByPopularity = sorted.map((value) => {
            const [name] = value;
            const product = products.find((product) => product.name === name);
            if (!product) {
                return undefined;
            }
            return Product.create({
                name,
                quantity: product.quantity,
                price: product.price,
            });
        });
        return productsByPopularity.filter((product) => product !== undefined) as Product[];
    }
}
