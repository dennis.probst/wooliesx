import Product from '../../../model/product';
import ShoppersHistory from '../../../model/shoppersHistory';
import { OrderProductBy } from '../../productService';
import SortProducts from '../sortProduct';

describe('SortProducts', () => {
    describe('byAttributes', () => {
        it.each([
            [
                'Defaults to order by Name Ascending',
                undefined,
                [Product.create({ name: 'b' }), Product.create({ name: 'A' })],
                Product.create({ name: 'A' }),
            ],
            [
                'Name Ascending',
                OrderProductBy.NameAscending,
                [Product.create({ name: 'b' }), Product.create({ name: 'A' })],
                Product.create({ name: 'A' }),
            ],
            [
                'Name Ascending name undefined',
                OrderProductBy.NameAscending,
                [
                    Product.create({ name: undefined }),
                    Product.create({ name: 'A' }),
                    Product.create({ name: undefined }),
                ],
                Product.create({ name: 'A' }),
            ],
            [
                'Name Descending',
                OrderProductBy.NameDescending,
                [Product.create({ name: 'a' }), Product.create({ name: 'B' })],
                Product.create({ name: 'B' }),
            ],
            [
                'Name Descending name undefined',
                OrderProductBy.NameDescending,
                [
                    Product.create({ name: undefined }),
                    Product.create({ name: 'z' }),
                    Product.create({ name: undefined }),
                ],
                Product.create({ name: 'z' }),
            ],
            [
                'Price Descending',
                OrderProductBy.PriceDescending,
                [Product.create({ price: 2 }), Product.create({ price: 1 })],
                Product.create({ price: 2 }),
            ],
            [
                'Price Descending price not defined',
                OrderProductBy.PriceDescending,
                [
                    Product.create({ price: undefined }),
                    Product.create({ price: 1 }),
                    Product.create({ price: undefined }),
                ],
                Product.create({ price: 1 }),
            ],
            [
                'Price Ascending',
                OrderProductBy.PriceAscending,
                [Product.create({ price: 1 }), Product.create({ price: 2 })],
                Product.create({ price: 1 }),
            ],
            [
                'Price Ascending price not defined',
                OrderProductBy.PriceAscending,
                [
                    Product.create({ price: undefined }),
                    Product.create({ price: 1 }),
                    Product.create({ price: undefined }),
                ],
                Product.create({ price: 1 }),
            ],
            [
                'When order not identified',
                'notDefined' as OrderProductBy,
                [
                    Product.create({ price: undefined }),
                    Product.create({ price: 1 }),
                    Product.create({ price: undefined }),
                ],
                Product.create({ price: undefined }),
            ],
        ])('sorts products by %s', (test, orderBy, products, expected) => {
            const [first] = SortProducts.byAttribute(products, orderBy);
            expect(first).toEqual(expected);
        });
    });

    describe('productsByPopularity', () => {
        const mockProducts: Product[] = [
            Product.create({ name: 'name1', quantity: 100, price: 1 }),
            Product.create({ name: 'name2', quantity: 200, price: 2 }),
        ];

        it('return products that has been bought most frequently first', () => {
            const shopperHistories = [
                ShoppersHistory.create({ products: [Product.create({ name: 'name1', quantity: 1, price: 1 })] }),
                ShoppersHistory.create({ products: [Product.create({ name: 'name2', quantity: 2, price: 2 })] }),
            ];
            const [first] = SortProducts.byPopularity(shopperHistories, mockProducts);
            expect(first).toEqual(Product.create({ name: 'name2', quantity: 200, price: 2 }));
        });

        it('accumulates products quantities from different shoppers', () => {
            const shopperHistories = [
                ShoppersHistory.create({ products: [Product.create({ name: 'name1', quantity: 1, price: 1 })] }),
                ShoppersHistory.create({ products: [Product.create({ name: 'name2', quantity: 2, price: 2 })] }),
                ShoppersHistory.create({ products: [Product.create({ name: 'name2', quantity: 2, price: 2 })] }),
            ];
            const [first] = SortProducts.byPopularity(shopperHistories, mockProducts);
            expect(first).toEqual(Product.create({ name: 'name2', quantity: 200, price: 2 }));
        });

        it('skips products without quantity or name', () => {
            const shopperHistories = [
                ShoppersHistory.create({ products: [Product.create({ name: 'name1', quantity: 1, price: 1 })] }),
                ShoppersHistory.create({
                    products: [Product.create({ name: 'name2', quantity: undefined, price: 2 })],
                }),
                ShoppersHistory.create({ products: [Product.create({ name: undefined, quantity: 2, price: 2 })] }),
            ];
            const result = SortProducts.byPopularity(shopperHistories, mockProducts);
            expect(result).toHaveLength(1);
            expect(result[0]).toEqual(Product.create({ name: 'name1', quantity: 100, price: 1 }));
        });

        it('skips products that are not available', () => {
            const shopperHistories = [
                ShoppersHistory.create({ products: [Product.create({ name: 'unknown', quantity: 1, price: 1 })] }),
            ];
            const result = SortProducts.byPopularity(shopperHistories, mockProducts);
            expect(result).toHaveLength(0);
        });
    });
});
