import Product from '../model/product';
import ShoppersHistory from '../model/shoppersHistory';

export interface ProductsResourceClient {
    listProducts(): Promise<Product[]>;

    listShopperHistories(): Promise<ShoppersHistory[]>;
}
