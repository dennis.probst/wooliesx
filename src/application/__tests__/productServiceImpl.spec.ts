import { mock } from 'jest-mock-extended';
import Product from '../../model/product';
import ShoppersHistory from '../../model/shoppersHistory';
import { OrderProductBy, ProductService } from '../productService';
import ProductServiceImpl from '../productServiceImpl';
import { ProductsResourceClient } from '../productsResourceClient';
import SortProduct from '../utility/sortProduct';

describe('productServiceImpl', () => {
    let productService: ProductService;
    const mockProductResourceClient = mock<ProductsResourceClient>();
    const mockProduct = Product.create({
        name: 'product name',
        price: 55,
        quantity: 4,
    });

    beforeEach(() => {
        jest.resetAllMocks();
        productService = new ProductServiceImpl(mockProductResourceClient);
    });

    describe('listProducts', () => {
        it('requests products from product resource', async () => {
            mockProductResourceClient.listProducts.mockResolvedValue([mockProduct]);
            SortProduct.byAttribute = jest.fn().mockReturnValue([mockProduct]);
            SortProduct.byPopularity = jest.fn();
            const products = await productService.listProducts({ orderBy: OrderProductBy.NameAscending });
            expect(mockProductResourceClient.listProducts).toHaveBeenCalled();
            expect(products).toEqual([mockProduct]);
        });

        it('returns an empty list, when no products found', async () => {
            mockProductResourceClient.listProducts.mockResolvedValue([]);
            const products = await productService.listProducts({ orderBy: OrderProductBy.NameAscending });
            expect(mockProductResourceClient.listProducts).toHaveBeenCalled();
            expect(products).toEqual([]);
        });

        describe('when ordered by attibute', () => {
            it('requests products be sorted by attribute', async () => {
                mockProductResourceClient.listProducts.mockResolvedValue([mockProduct]);
                SortProduct.byAttribute = jest.fn().mockReturnValue([mockProduct]);
                SortProduct.byPopularity = jest.fn();
                await productService.listProducts({ orderBy: OrderProductBy.NameAscending });
                expect(SortProduct.byAttribute).toHaveBeenCalledWith([mockProduct], OrderProductBy.NameAscending);
                expect(SortProduct.byPopularity).not.toHaveBeenCalled();
            });
        });

        describe('when ordered by popularity', () => {
            const mockShopperHistory = ShoppersHistory.create({
                customerId: 1,
                products: [Product.create({ name: 'product1', price: 2, quantity: 3 })],
            });

            it('requests shopper history be sorted by popularity', async () => {
                mockProductResourceClient.listProducts.mockResolvedValue([mockProduct]);
                mockProductResourceClient.listShopperHistories.mockResolvedValue([mockShopperHistory]);
                SortProduct.byAttribute = jest.fn();
                SortProduct.byPopularity = jest.fn().mockReturnValue([mockProduct]);
                await productService.listProducts({ orderBy: OrderProductBy.Popularity });
                expect(mockProductResourceClient.listShopperHistories).toHaveBeenCalled();
                expect(SortProduct.byPopularity).toHaveBeenCalledWith([mockShopperHistory], [mockProduct]);
                expect(SortProduct.byAttribute).not.toHaveBeenCalled();
            });
        });
    });
});
