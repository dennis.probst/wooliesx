import { ProductQuantity, Special, Trolley, TrolleyProduct } from '../../model/trolley';
import { TrolleyService } from '../trolleyService';
import TrolleyServiceImpl from '../trolleyServiceImpl';

describe('trolleyServiceImpl', () => {
    let trolleyService: TrolleyService;

    beforeEach(() => {
        trolleyService = new TrolleyServiceImpl();
    });

    describe('calculateTotal', () => {
        const mockProducts: TrolleyProduct[] = [
            { name: 'product1', price: 100 },
            { name: 'product2', price: 200 },
            { name: 'product3', price: 300 },
        ];

        it('returns max total when no applicable special', () => {
            const trolley: Trolley = {
                quantities: [{ name: 'product1', quantity: 1 }],
                specials: [],
                products: mockProducts,
            };
            const total = trolleyService.calculateTotal(trolley);
            expect(total).toBe(100);
        });

        it('applies applicable special', () => {
            const trolley: Trolley = {
                quantities: [{ name: 'product1', quantity: 2 }],
                specials: [{ quantities: [{ name: 'product1', quantity: 2 }], total: 50 }],
                products: mockProducts,
            };
            const total = trolleyService.calculateTotal(trolley);
            expect(total).toBe(50);
        });

        it('uses max value when lower than applicable special', () => {
            const trolley: Trolley = {
                quantities: [{ name: 'product1', quantity: 2 }],
                specials: [{ quantities: [{ name: 'product1', quantity: 2 }], total: 300 }],
                products: mockProducts,
            };
            const total = trolleyService.calculateTotal(trolley);
            expect(total).toBe(200);
        });

        it('uses lowest special value when multiple matching', () => {
            const trolley: Trolley = {
                quantities: [{ name: 'product1', quantity: 18 }],
                specials: [
                    { quantities: [{ name: 'product1', quantity: 11 }], total: 60 },
                    { quantities: [{ name: 'product1', quantity: 5 }], total: 50 },
                    { quantities: [{ name: 'product1', quantity: 2 }], total: 40 },
                ],
                products: mockProducts,
            };
            const total = trolleyService.calculateTotal(trolley);
            expect(total).toBe(150);
        });

        it('uses multiple special value with quantities of same product', () => {
            const trolley: Trolley = {
                quantities: [
                    { name: 'product1', quantity: 3 },
                    { name: 'product2', quantity: 1 },
                    { name: 'product3', quantity: 1 },
                ],
                specials: [
                    {
                        quantities: [
                            { name: 'product1', quantity: 1 },
                            { name: 'product2', quantity: 1 },
                        ],
                        total: 50,
                    },
                    {
                        quantities: [
                            { name: 'product1', quantity: 1 },
                            { name: 'product3', quantity: 1 },
                        ],
                        total: 40,
                    },
                ],
                products: mockProducts,
            };
            const total = trolleyService.calculateTotal(trolley);
            expect(total).toBe(190);
        });

        it('uses lowest multiple special value with overlapping quantities of same product', () => {
            const trolley: Trolley = {
                quantities: [
                    { name: 'product1', quantity: 5 },
                    { name: 'product2', quantity: 5 },
                    { name: 'product3', quantity: 5 },
                ], // total 3000
                specials: [
                    {
                        quantities: [
                            { name: 'product1', quantity: 3 }, // 300
                            { name: 'product2', quantity: 2 }, // 400
                        ],
                        total: 51,
                    },
                    {
                        quantities: [
                            { name: 'product1', quantity: 3 }, // 300
                            { name: 'product3', quantity: 2 }, // 600
                        ],
                        total: 52,
                    },
                ],
                products: mockProducts,
            };
            const total = trolleyService.calculateTotal(trolley);
            expect(total).toBe(2152);
        });
    });

    describe('not Interfaced Methods', () => {
        const trolleyServiceImpl = new TrolleyServiceImpl();

        describe('applySpecial', () => {
            it('returns a new productQuanitity list with special quantities substracted', () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 1 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                const [{ quantity }] = trolleyServiceImpl.applySpecial(productQuants, special);
                expect(quantity).toBe(0);
            });

            it('does not alter the passed array of product quantities', () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 1 }], total: 1 };
                const productQuantities: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                trolleyServiceImpl.applySpecial(productQuantities, special);
                expect(productQuantities).toEqual([{ name: 'product1', quantity: 1 }]);
            });

            it("throws when special quantity can't be applied", () => {
                expect.assertions(1);
                const special: Special = { quantities: [{ name: 'product1', quantity: undefined }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                try {
                    trolleyServiceImpl.applySpecial(productQuants, special);
                } catch (err) {
                    expect(err.message).toBe('this is an error, special not applied');
                }
            });

            it('throws when productQuantitie are undefined', () => {
                expect.assertions(1);
                const special: Special = { quantities: [{ name: 'product1', quantity: 3 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'different', quantity: 3 }];
                try {
                    trolleyServiceImpl.applySpecial(productQuants, special);
                } catch (err) {
                    expect(err.message).toBe('this is an error, special not applied');
                }
            });

            it('throws when productQuantity.quantity are undefined', () => {
                expect.assertions(1);
                const special: Special = { quantities: [{ name: 'product1', quantity: 3 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: undefined }];
                try {
                    trolleyServiceImpl.applySpecial(productQuants, special);
                } catch (err) {
                    expect(err.message).toBe('this is an error, special not applied');
                }
            });

            it('throws when productQuantity.quantity is too low', () => {
                expect.assertions(1);
                const special: Special = { quantities: [{ name: 'product1', quantity: 3 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                try {
                    trolleyServiceImpl.applySpecial(productQuants, special);
                } catch (err) {
                    expect(err.message).toBe('this is an error, special not applied');
                }
            });
        });

        describe('isApplicable', () => {
            it('returns true when a special can be applied to productQuantities', () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 1 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeTruthy();
            });

            it('skips special when total is missing', () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 1 }], total: undefined };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeFalsy();
            });

            it("skips special when quantity is missing in one of it's quantities", () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: undefined }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeFalsy();
            });

            it("true special when quantity is 0 in one of it's quantities", () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 0 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeTruthy();
            });

            it("skips special when name is missing in one of it's quantities", () => {
                const special: Special = { quantities: [{ name: undefined, quantity: 1 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 1 }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeFalsy();
            });

            it('false when name is missing in product quantity', () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 1 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: undefined, quantity: 1 }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeFalsy();
            });

            it('false when quantity is missing in product quantity', () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 1 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: undefined }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeFalsy();
            });

            it('true when quantity is missing in product quantity', () => {
                const special: Special = { quantities: [{ name: 'product1', quantity: 0 }], total: 1 };
                const productQuants: ProductQuantity[] = [{ name: 'product1', quantity: 0 }];
                expect(trolleyServiceImpl.isApplicable(productQuants, special)).toBeTruthy();
            });
        });
    });

    describe('calculateMax', () => {
        it.todo('calculates max of quantities');

        it.todo('handles when quantity for product not in list');

        it.todo('handles when quantity for product not defined');

        it.todo('handles when price for product not defined');
    });
});
