import { getLogger } from '../logger';
import Product from '../model/product';
import { OrderProductBy, ProductService } from './productService';
import { ProductsResourceClient } from './productsResourceClient';
import SortProduct from './utility/sortProduct';

const logger = getLogger('productServiceImpl');

export default class ProductServiceImpl implements ProductService {
    constructor(private productResourceClient: ProductsResourceClient) {}

    async listProducts(params: { orderBy: OrderProductBy }): Promise<Product[]> {
        const { orderBy } = params;
        logger.info('listProducts request ordered by %s', orderBy);
        const products = await this.productResourceClient.listProducts();
        if (params.orderBy === OrderProductBy.Popularity) {
            const shopperHistory = await this.productResourceClient.listShopperHistories();
            return SortProduct.byPopularity(shopperHistory, products);
        }
        return products.length ? SortProduct.byAttribute(products, orderBy) : [];
    }
}
