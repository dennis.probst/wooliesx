import { Trolley } from '../model/trolley';

export interface TrolleyService {
    calculateTotal(trolley: Trolley): number;
}
