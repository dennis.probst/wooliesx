import Product from '../model/product';

export interface ProductService {
    listProducts(params?: { orderBy: OrderProductBy }): Promise<Product[]>;
}

export enum OrderProductBy {
    PriceAscending = 'Low',
    PriceDescending = 'High',
    NameAscending = 'Ascending',
    NameDescending = 'Descending',
    Popularity = 'Recommended',
}
