import PinoHttp from 'express-pino-logger';
import pino, { Logger } from 'pino';
import { appConfig } from './config';

const logger = pino({
    mixin() {
        return {
            service: 'code-test-api',
        };
    },
    base: null,
    level: appConfig.logLevel,
});

export function expressPinoLogger(): PinoHttp.HttpLogger {
    return PinoHttp({ logger, autoLogging: { ignorePaths: ['/health'] } });
}

export function getLogger(moduleName: string): Logger {
    return logger.child({ module: moduleName });
}
