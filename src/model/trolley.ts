export interface Trolley {
    products: TrolleyProduct[];
    specials: Special[];
    quantities: ProductQuantity[];
}

export interface TrolleyProduct {
    name?: string;
    price?: number;
}

export interface Special {
    quantities?: ProductQuantity[];
    total?: number;
}

export interface ProductQuantity {
    name?: string;
    quantity?: number;
}
