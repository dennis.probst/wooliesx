/* istanbul ignore file: for code exercise */
import { StatusCode } from '../statusCode';

export default class InvalidArgumentError extends Error {
    readonly status: StatusCode;

    constructor(message?: string) {
        super(message);
        this.name = 'InvalidArgumentError';
        this.status = StatusCode.InvalidArgument;
    }
}
