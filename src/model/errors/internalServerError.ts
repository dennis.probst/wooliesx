import { StatusCode } from '../statusCode';

export default class InternalServerError extends Error {
    readonly status: StatusCode;

    constructor(message?: string) {
        super(message);
        this.name = 'InternalServerError';
        this.status = StatusCode.InternalServerError;
    }
}
