import Product from './product';

export default class ShoppersHistory {
    constructor(readonly customerId?: number, readonly products?: Product[]) {}

    // todo: test
    static create(params: { customerId?: number; products?: Product[] }): ShoppersHistory {
        const { customerId, products } = params;
        return new ShoppersHistory(customerId, products);
    }
}
