export enum StatusCode {
    Ok = 200,
    InvalidArgument = 400,
    NotFound = 404,
    InternalServerError = 500,
}
