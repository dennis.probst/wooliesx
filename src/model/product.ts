export default class Product {
    constructor(readonly name?: string, readonly price?: number, readonly quantity?: number) {}

    // todo test
    static create(params: { name?: string; price?: number; quantity?: number }): Product {
        const { name, price, quantity } = params;
        return new Product(name, price, quantity);
    }
}
