/* istanbul ignore file: for code exercise */
import App from './infra/app';
import { getLogger } from './logger';

const logger = getLogger('server');

const app = new App();

app.start((error?: Error | null, port?: number) => {
    if (error) {
        logger.error('Server error %s: %s', error, error.stack);
        process.exit(1);
    }
    logger.info('Server running on port %d', port);
});
