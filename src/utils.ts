/* istanbul ignore file: for code exercise */
export function compareNumbers(a: number, b: number): number {
    if (a > b) {
        return 1;
    }
    if (a < b) {
        return -1;
    }
    return 0;
}
