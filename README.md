# Woolies X

# Notes
* Infrastructure code and some utils are not tested for this exercise, but I would expect that to be in place to consider it prod ready. I just want to limit the amount of work for this exercise.
* Exercise 2: Recommended. I'm miss-understanding something about the definition of what is highest in popularity. My implementation is accumulating the total quantities of each product across all customer purchases and uses that as scoring for product popularity to sort the list of products from the product endoint as they have the correct quantity available. 
  I return in order with the highest popularity first. I tried the other way but that is not the right answer.
* Data posted by the server for the last exercise does not align with the OpenApi spec. The attributes in the objects in the spec are lower case the actual data uses capitals. I've adjusted to that.
* One of the tests in test 3 sends float point numbers resulting in rounding errors. That is a binary number problem of computers. I should have used other data structures for those numbers from the start.
  There are two ways to solve this. Just too much to change in the code now for this exercise. 
    * simple fix is to multiply by a factor to make all numbers integers and the computer can handle it internally
    * use specific data structures such as BigDecimal/BigNumber which is the better option for a use case using money

# Prerequisites

- [Node.js and NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

# Getting started

Run
```bash
npm i
```
to load dependencies

Build and run the server
```bash
npm run build
npm start
```

# Tests
Run
```bash
npm test
```

